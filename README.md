## PRE-PROCESSING OF HISTORICAL AERIAL PHOTOGRAPHS  
*Last modified: 5th Feb. 2020*  

The present repository contains a series of scripts that are useful to prepare datasets of scanned aerial photographs before their photogrammetric processing. The goal here is to go from raw scanned images to sets of images that have **1)** identical pixel dimensions in width and height, and **2)** the center of perspective relocated at the center of the image, based on the fiducial marks (i.e., = interior orientation).  
  
All these scripts were developed in the frame of the **PAStECA Project** (BELSPO, BRAIN-be Programme, Contract n° BR/165/A3/PASTECA, http://pasteca.africamuseum.be/). They were written in Python 3.7, on MacOS, but were also tested on Windows 10 Pro.
  
Each script can be run as it, by simply adapting the necessary parameters. All these scripts have the same 4-part structure:  
  
*1) A header providing a description of the script, the name of the authors, the required Python modules, the reference to cite if used, etc.*  
*2) A section with the Python modules to install (in addition to the Anaconda Package distribution)*  
*3) A section with the variables and parameters that must be adapted by the user (that is the only part that must be modified before use)*  
*4) The required coding to perform the task (should not be modified, except if you want to tune the script for your specific needs)*  
  
To get the required Python module, the followed philosophy was to install the Anaconda Distribution for Python 3 and add the missing modules using "conda install" or "pip install" functions. The required Python modules that are not installed by default with Anaconda are mentioned in the header of the script.  
  
Each script has been optimized for speed by parallelizing the job using the Multiprocessing module.  
  
The scripts has also been adapted to display information about the ongoing processing, in the Python console or terminal.  
  
The Python scripts currently provided are attached to the following reference:  
**Authors:** *Smets, B., Dewitte, O., Michellier, C., Muganga, G., Dille, A., Kervyn, F.*  
**Title:** *Time-series of orthomosaics of Goma, D.R. Congo, from 1947 to 1958*  
**Journal:** *Data (submitted)*  
Please, cite this reference in your work, if you use the scripts. Thank you in advance! (The publication status will be adapted asap) 

All the Python scripts provided here are created by an Earth Scientist with self-learned programming skills. They can definitely be improved by a professional programmer. So, I already apologize for that.  
  
If you have constructive comments that could help me to improve or speed up these scripts, please do not hesitate to contact me! I thank you in advance!  
  
**Benoît Smets**   
*Postdoc Researcher in Remote Sensing, Photogrammetry, Geohazards, Volcanology*  
Royal Museum for Central Africa, Belgium  
  
To contact me --> MyFirstName.MyLastName@africamuseum.be  
  
    
-----  
  
**!!! PLEASE READ THE FOLLOWING DESCRIPTION BEFORE USING THE SCRIPTS FOR THE FIRST TIME !!!**  

## SCRIPT 0: Copy2singleFolder (optional)
*Current version:* **1.0**

This script simply copies all the raw scanned photos that are stored in a series of subfolders into a single folder. It has been developed as the technicians who are taking care of scanning and archiving the raw scanned photos follow a specific structure of folders and subfolders to store the data. As all the following scripts are based on the assumption that all the photos to process are in the same folder (i.e., one directory path provided for the input data), this small script allows copying the scanned data into an appropriate single place for their preprocessing. It also has the advantage of leaving the original scans where they are, and making us working on a copy of them.

The required Python modules are **os** and **shutil**. They are both provided by default.

**The parameters to provide are:**  
*- The path of the source folder (i.e., master folder where all the photos and subfolders are located)*  
*- The path of the destination folder*  
*- The file format of the photos (by default, .tif), in case other files are stored in the folder and subfolders*  

## SCRIPT 1: AirPhoto_CanvasSizing 
*Current version:* **1.0**  
  
This script aims to get images with the same number of pixels in width and height, which is not always the case with scanned photographs. The script will look at all photographs available in a given directory and search for the maximum width and height values in the dataset. Once found, it will homogenize the dataset by adding rows and/or columns of black pixels to images that don't have these maximum dimensions.  
  
**The required Python modules to add (in addition to the Anaconda Distribution):**  
*- Glob*  
*- OpenCV*  
*- joblib*  
  
**The parameters that have to be adapted by the user:**  
*- Input folder (where the raw scans are located)*  
*- Output folder (where the resized images will be saved)*  
*- The image format of the input images (e.g., tif, jpeg, png, etc.)*  
*- The number of CPU cores to use for the parallel processing (by default: max - 1)*  
  
The output images will be saved with the same name as the input images, complemented with "_CanvasSized". The images will be saved in tif format, as I only work with raw (uint16) tif files. If you want to change this, you have to adapt the file format in the script, in line 107.  
  
  
## SCRIPT 2: AirPhoto_reprojection  
*Current version:* **1.0**  
  
This script aims to reproject the aerial photographs based on the pixel coordinates of the fiducial marks, in order to obtain an homogeneous dataset with the center of perspective centered in the middle of the images. To run this script, you first need to create a table, in csv format, containing the XY coordinates (in pixel) of four fiducial marks used to locate the center of perspective. A template of such a table is provided. Please, keep the name of each columns similar to those in the template, as these names are used in the script to find the corresponding information.  
  
**The required Python modules to add (in addition to the Anaconda Distribution):**  
*- Glob*  
*- OpenCV*  
*- joblib*  
  
**The parameters that have to be adapted by the user:**  
*- Input folder (with the "canvas-sized" images)*  
*- Directory path of the csv file with the pixel coordinates of the fiducial marks*  
*- Output folder (where the standardized images will be saved)*  
*- The new pixel coordinates of the fiducial marks in the output images (must be estimated based on the type of photo and the scanning resolution)*  
*- The dimensions in width (X) and height (Y) of the output images (unit = pixel)*  
*- The image format of the input images (e.g., tif, jpeg, png, etc.)*  
*- The number of CPU cores to use for the parallel processing (by default: max - 1)*  
*- OPTIONAL: at this stage, you can create image masks for images having fiducial marks visible in the corners of the photographs. By default, this option is not activated, as these masks have to be downsampled before use, while Scripts 3a and 3b allow the production of image masks with the final resolution. The image masks are, by default, saved in png format.*  
  
The output images will be saved with the same name as the input images, complemented with "_standardized". The images will be saved in tif format, as I only work with raw (uint16) tif files. If you want to change this, you have to adapt the file format in the script, in line 124 (line 167 for the image masks).

## Downsampling of the images

In order to smooth the noise introduced by the reprojection of each image, **I strongly suggest to downsample the images to a lower resolution**. At the RMCA, we scan the photos at a resolution of 1500 dpi (except for specific collections), which is, in general, too much considering the quality of the collections. So, we use to resample the reprojected photos to 900 dpi (+ or - 300, depending on the quality of the dataset).

After several tests with different resampling algorithms and Python modules, it appears that **the best resampling method to obtain a good photogrammetric result is the "Bicubic Sharper" algorithm of Adobe Photoshop(R)**. This algorithm is by far better than any other available for Python. So I strongly recommend to use Photoshop for this step.

If, in any case, you find an open-source equivalent of the Bicubic Sharper algorithm of Photoshop, please do not hesitate to share this information with me! Thank you in advance!

## SCRIPT 3a: CreateMask (optional)
*Current version:* **1.0**  

This script aims to create a mask with the same dimensions of the preprocessed photos (i.e., photos sized, reprojected and downsampled). It is planned to mask rectangles at the corners of the images, in order to hide the fiducial marks that are still visible on the final photos. In a future version of the script, an option will be added to hide fiducial marks that are at mid-distance between the corners of the image.

**The required Python modules to add (in addition to the Anaconda Distribution):**  
*- Glob*   
*- joblib*  
  
**The parameters that have to be adapted by the user:**  
*- Input folder (with the reprojected photos)*  
*- Output mask folder (where the masks will be stored)*  
*- The image format (of the reprojected photos)*  
*- The size of the rectangles to mask in the corners of the photos (in percentage of the photo's width and height)*  
*- The number of CPU cores to use for the parallel processing (by default: max - 1)*  

The output masks will be saved with the same name as the input images, complemented with "_mask". The mask images will be saved in png format, as Agisoft Photoscan/Metashape Pro preferentially works with this format for masks. If you want to change this, you have to adapt the mask format in the script, in line 140.

**Important note:**  
The main disadvantage of this script is that it creates a mask file per photo, which might be a problem for hard disk space in case of large datasets. To avoid this problem, it is possible to rather use Script 3b (CreateSingleMask).

## SCRIPT 3b: CreateSingleMask (optional)
*Current version:* **1.0**  

This script is only useful for photogrammetric software that allows you to apply a single mask file to all the photos of the same dimensions, like with Agisoft Photoscan/Metashape Pro. It is very useful to avoid unnecessarily losing hard disk space, like it is the case with Script 3a (CreateMask).

This script is similar to Script 3a, in a way that it also masks rectangles at the corners of the images. In a future version of the script, an option will be added to hide fiducial marks that are at mid-distance between the corners of the image.

**The required Python modules to add (in addition to the Anaconda Distribution):**  
*- Glob*   

**The parameters that have to be adapted by the user:**  
*- Input folder (with the reprojected photos)*  
*- Output mask folder (where the mask will be stored)*  
*- The image format (of the reprojected photos)*  
*- The size of the rectangles to mask in the corners of the photos (in percentage of the photo's width and height)*  
*- The name of the dataset (i.e., the name you will give to the mask)*  

The output mask will be saved with the given name of the dataset, complemented with "_mask". The mask will be saved in png format, as Agisoft Photoscan/Metashape Pro preferentially works with this format for masks. If you want to change this, you have to adapt the mask format in the script, in line 134.


-----

**Dr. Benoît SMETS**  
Natural Hazards Service  
ROYAL MUSEUM FOR CENTRAL AFRICA (Belgium)  
https://georiska.africamuseum.be/en  
http://www.virunga-volcanoes.org/  
https://bsmets.net/  